<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY version "0.1.0">
]>
<article id="telegnome" lang="zh-CN">
  <articleinfo>
    <title>TeleGNOME 用户手册</title>
    <abstract role="description">
      <para>TeleGNOME 从网络上下载和显示图文电视</para>
    </abstract>
    <authorgroup>
      <author>
	<firstname>Arjan</firstname> <surname>Scherpenisse</surname>
	<affiliation>
	  <address>
	    <email>acscherp@wins.uva.nl</email>
	  </address>
	</affiliation>
      </author>
      <author>
	<firstname>Dirk-Jan</firstname> <surname>Binnema</surname>
	<affiliation>
	  <address>
	    <email>djcb@dds.nl</email>
	  </address>
	</affiliation>
      </author>
      <author>
	<firstname>Ork</firstname> <surname>de Rooij</surname>
	<affiliation>
	  <address>
	    <email>ork.derooij@student.uva.nl</email>
	  </address>
	</affiliation>
      </author>
      <author role="maintainer">
	<firstname>Colin</firstname> <surname>Watson</surname>
	<affiliation>
	  <address>
	    <email>cjwatson@debian.org</email>
	  </address>
	</affiliation>
      </author>
    </authorgroup>
    <copyright>
      <year>1999, 2000</year> <holder>Arjan Scherpenisse</holder>
    </copyright>
    <copyright>
      <year>2008</year> <holder>Colin Watson</holder>
    </copyright>
    <legalnotice>
      <para>本文档是自由软件；您可以按照自由软件基金会所发表的 GNU GPL 协议自由发放和/或修改它；GPL 协议应该采用第二版或以后的任何版本。</para>
      
      <para>本程序发表的目的是希望它能够对您有用，但我们没有任何保证；对于以任何用途使用它所造成的任何直接或间接后果都不承担任何责任。请参看GNU GPL 协议中的细节。</para>
      
      <para>You should have received a copy of the GNU General Public
	License along with this program; if not, see
	<ulink type="http" url="http://www.gnu.org/licenses/"/></para>
      
      <para>更多细节，请查看 GNOME 源代码中的 COPYING 文件。</para>
    </legalnotice>
    <revhistory>
      <revision>
	<revnumber>TeleGNOME User's Guide 0.1.0</revnumber>
	<date>2008-04-27</date>
      </revision>
    </revhistory>
  </articleinfo>
 
  <sect1 id="intro">
    <title>简介</title>
    <para>TBD.</para>
  </sect1>    
  
  <sect1 id="ui">
    <title>用户界面</title> 
    
    <para>主界面由菜单栏、工具栏、当前视图页和状态栏组成。</para>
    
    <sect2>
      <title>菜单</title> 

      <para>菜单栏由 3 个菜单组成，称为“文件”、“设置” 和“帮助”。这些菜单中的每一个都有如下选项可用。</para>
      
      <itemizedlist>
	<listitem>
	  <para>文件</para>
	</listitem>
	<listitem>
	  <para>设置</para>
	</listitem>
	<listitem>
	  <para>频道</para>
	</listitem>
	<listitem>
	  <para>帮助</para>
	</listitem>
      </itemizedlist>
    </sect2>
  </sect1>

  <sect1 id="using">
    
    <title>使用 TeleGNOME</title>
    
    <para>Normal teletext consists of a series of pages, which are
    transmitted in quick succession by the broadcaster. Whenever you
    want your TV to tune in on a specific page, your TV will capture
    that page only and show it on the screen.</para>
  
    <para>Every page can have subpages, that is: the page consists
    of a couple of subpages. This way the numbering of pages can
    stay the same, but the amount of information in a page can vary.
    For example: the page that serves the news (101 on Dutch TV) can
    have just a few items of information (nothing happened in the
    world), and thus have no subpages, or it can have a lot of
    items, spread out on subpages.  These subpages are usually
    automatically updated on your TV, timed by an interval in which
    the page can be read. This means that if you want to see subpage
    7 and the current displayed subpage is 3, you'll have to wait
    some time. This can be very annoying. Also, if the timing is to
    fast, the page may have disappeared before you've read it, which
    means that you'll have to wait for the page to come up
    again.</para>
  
    <para>Using <application>TeleGNOME</application> is very similar to using teletext on any
    regular television set, but without the sometimes irritating
    long waits for a page to come up.</para>

    <para>Just like teletext on a TV, you can simply type a page
    number and press <keycap>ENTER</keycap> to load that page. This
    number can be any number served by the current teletext channel,
    and it is not restricted to the numbers shown on the current
    page.</para>

    <para>You can load subpages directly by typing the page number, a
    dash or a slash and the subpage number. For example: if you want
    to load the second page of page 201, just type
    <keycap>201/2</keycap>.</para>

    <para>When you're ready to read the next subpage (if any) you
    can press the <keycap>next</keycap> button. This will load the
    next subpage. Alternatively you can just enter the requested
    subpage immediately using the keyboard.</para>
  </sect1>
</article>
